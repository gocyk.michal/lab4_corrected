#include <iostream>
#include <fstream>
#include <vector>
#include <conio.h>
#include <time.h>

using namespace std;
time_t now, later;
 
void sleep(int delay)
{
 now=time(NULL);
 later=now+delay;
 while(now<=later)now=time(NULL);
}


class programs
{
	public:
	int id;
	string names;
	int time;
	int temperature;
	int power;
	
	programs (int xid, string xnames, int xtime, int xtemp, int xpower);
	programs (string xnames, int xtime, int xtemp, int xpower);
			
	void print()
	{
		cout<<id<<"."<<names<< " - time: "<< time<<" [min];"<<" temperatue: "<<temperature<<" [C];"<< " power consumption: " << power<<" [W];"<< "\n";
	}	
};


class lastProgram : public programs
{
	public:
	lastProgram (string xnames, int xtime, int xtemp, int xpower);
	
	void print()
	{
		cout<<names<< " - time: "<< time<<" [min];"<<" temperatue: "<<temperature<<" [C];"<< " power consumption: " << power<<" [W];"<< "\n";
	}
};


void read(fstream & program, vector<programs> & tab_programs)
{
	int xid;
	string xnames;
	int xtime;
	int xtemp;
	int xpower;
	program.open("programs.txt", ios::in);

	for( int i = 0; i<4; i++)
	{
		program>>xid;
		program>>xnames;
		program>>xtime;
		program>>xtemp;
		program>>xpower;
		tab_programs.push_back(programs (xid, xnames, xtime, xtemp, xpower));

	}
	program.close();
}

void read_last(fstream & program_last, vector<lastProgram> & tab_lastProgram)
{
	string xnames;
	int xtime;
	int xtemp;
	int xpower;
	program_last.open("last_program.txt", ios::in);

	program_last>>xnames;
	program_last>>xtime;
	program_last>>xtemp;
	program_last>>xpower;
	tab_lastProgram.push_back(lastProgram (xnames, xtime, xtemp, xpower));

	program_last.close();
}


void print_programs(vector<programs> & tab_programs)
{
		for( int i = 0; i<tab_programs.size(); i++)
		{
			tab_programs[i].print();
		}
}



void print_last(vector<lastProgram> & tab_lastProgram)
{
	tab_lastProgram[0].print();
}

programs::programs(int xid, string xnames, int xtime, int xtemp, int xpower)
{
	id = xid;
	names = xnames;
	time = xtime;
	temperature = xtemp;
	power = xpower;
}

programs::programs(string xnames, int xtime, int xtemp, int xpower)
{
	names = xnames;
	time = xtime;
	temperature = xtemp;
	power = xpower;	
}

lastProgram::lastProgram(string xnames, int xtime, int xtemp, int xpower)
	: programs(xnames, xtime, xtemp, xpower)
{

}

main()
{
	fstream program;
	vector<programs> tab_programs;
	read(program, tab_programs);
	fstream program_last;
	vector<lastProgram> tab_lastProgram;
	
	int a=0;
	int b;
	int button;
	do
	{
		
		menu:
		print_programs(tab_programs);
		cout<<"\n"<<"The recently used program is:"<<"\n";
		read_last(program_last, tab_lastProgram);
		print_last(tab_lastProgram);
		cout<<"\n"<<"Choose program: ";
		cin>>a;
		
				
		system("cls");
		cout<<"The chosen program is "<<tab_programs[a-1].names<< " - time: "<< tab_programs[a-1].time <<" [min];"<<" temperatue: "<<tab_programs[a-1].temperature <<" [C];"<< " power consumption: " << tab_programs[a-1].power<<" [W]"<<"."<<"\n"<<"\n";
		cout<<"Press any key to start"<<"\n";
		getch();
		
			fstream program_last;
			
			program_last.open("last_program.txt"); 
			
			if(program_last.good() == true)
			
			{
				program_last <<tab_programs[a-1].names<<" "<<tab_programs[a-1].time<<" "<<tab_programs[a-1].temperature<<" "<<tab_programs[a-1].power<<"\n";
        		program_last.close();
			}
			
			
	}
	while(a<0 || a>6);

	a--;
	system("cls");
	cout<<"Running"<<"\n";
	button = 0;

	do
	{
		if(kbhit())
		{	
		button = getch();
		}
			
		if( button == 112 )
		{
		cout<<tab_programs[a].time<<" minutes left."<<"\n";
		
		tab_programs[a].time = tab_programs[a].time;
		cout<<"PAUSED"<< "\n"<<"press any key to resume"<<"\n";
		sleep(0.5);
		system("cls");
		}
		
		else{
			cout<<tab_programs[a].time<<" minutes left"<<"\n";
			cout<<"press p to pause"<<"\n"<<"press s to stop";
		
		tab_programs[a].time = 	tab_programs[a].time - 1;
		sleep(0.5);
		system("cls");
			}			
	}
		
	
	while(tab_programs[a].time > 0 && button != 115);

	cout<<"The washing has ended."<<"\n";
	sleep(1.5);
	system("cls");

	goto menu;
	
	
return 0;
}

